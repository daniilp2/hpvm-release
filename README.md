# The HPVM Compiler Infrastructure

[![Documentation Status](https://readthedocs.org/projects/hpvm/badge/?version=latest)](https://hpvm.readthedocs.io/en/latest/?badge=latest)
[![pipeline status](https://gitlab.engr.illinois.edu/llvm/hpvm-release/badges/main/pipeline.svg)](https://gitlab.engr.illinois.edu/llvm/hpvm-release/-/commits/main)

This repository contains the source code and documentation for the HPVM Compiler Infrastructure.

HPVM is a compiler for heterogeneous parallel system.
For more about what HPVM is, see [our website](https://publish.illinois.edu/hpvm-project/)
and publications:
[PPoPP'18 paper](https://dl.acm.org/doi/pdf/10.1145/3200691.3178493),
[OOPSLA'19 paper](https://dl.acm.org/doi/10.1145/3360612),
[PPoPP'21 paper](https://dl.acm.org/doi/10.1145/3437801.3446108).

Read our [online documentation](https://hpvm.readthedocs.io/en/latest/)
for how to build, install, and use HPVM.

HPVM is currently at **version 1.0**.

## Support

All questions can be directed to [hpvm-dev@lists.cs.illinois.edu](mailto:hpvm-dev@lists.cs.illinois.edu).
